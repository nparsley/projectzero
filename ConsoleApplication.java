package com.example;

import java.util.List;
import java.util.Scanner;

import com.example.MenuScreen.Action;
import com.example.dao.UserDaoImpl;

public class ConsoleApplication {
	private User currentUser;
	
	public static void main(String[] args) {
		new ConsoleApplication();
	}
	
	public ConsoleApplication() {
		
		UserDaoImpl udi = new UserDaoImpl();
		
		Scanner conScan = new Scanner(System.in);

		Bank bank = new Bank();

		Screen presentScreen = new MenuScreen();
		System.out.println("Welcome to the bank");
		boolean keepGoing = true;
		while (keepGoing) {

			Action action = (Action) presentScreen.getUserInput(conScan);
			switch (action) {
			case ADD_USER: {
				Screen screen = new AddUserScreen();
				User user = (User) screen.getUserInput(conScan);
				bank.addUser(user);
				udi.insertUserObject(user);
				break;
			}
			case DELETE_ACCOUNT: {
				if(currentUser.isAdmin()) {
					udi.deleteAccount();
				}
				break;
			}
			case DEPOSIT_FUNDS: {
//				System.out.println(currentUser.getUserType() + " " + currentUser.isCustorAd());
				if (currentUser.isCustorAd()) {
					udi.accountActions(currentUser.getUserName());
//					System.out.println("How much would you like to deposit? ");
//					Scanner sc = new Scanner(System.in);
//					String userInput = sc.nextLine();
//					account.depositAmount(Double.parseDouble(userInput));
				}

				break;
			}
			case APPLY_ACCOUNT: {
				Screen screen = new ApplyAccountScreen();
				String[] userNameAndAccount = (String[]) screen.getUserInput(conScan);
				String userName = userNameAndAccount[0];
				String accountName = userNameAndAccount[1];
//				User user = bank.getUser(userName);
//				user.applyAccount(accountName);
				udi.openBankAccount(userName, accountName);
				break;
			}
			case APPROVE_ACCOUNT: {
				if (currentUser.isEmployee()) {
					Screen screen = new ApproveAccountScreen();
					String[] info = (String[]) screen.getUserInput(conScan);
					String approverUserName = info[0];
					String approveeUserName = info[1];
					String bankAccountName = info[2];
				} else {
					System.out.println("await account approval");
				}

				break;
			}
			case PRINT_ACCOUNT_INFO: {
				BankAccount account = udi.printList();
				System.out.println(account);
				break;
			}
			case QUIT: {
				keepGoing = false;
				break;
			}
			case TRANSFER_FUNDS: {
				break;
			}
			case WITHDRAW_FUNDS: {
				if(currentUser.isCustorAd()) {
					udi.withdrawActions(currentUser.getUserName());
				}
				break;
			}
			case LOGIN_USER: {
//				Screen screen = new LoginUserScreen();
//				User loginUser = (User) screen.getUserInput(conScan);
				System.out.print("Please enter username: ");
				String username = conScan.nextLine();
				System.out.print("Please enter password: ");
				String password = conScan.nextLine();
				
//				String passwordN = loginUser.getPassword();
				User dbUser = udi.getUser(username);
				if (password.equals(dbUser.getPassword())) {
					currentUser = dbUser;
				} else {
					System.out.println("incorrect password, user not logged in.");
				}
//				bank.loginUser(loginUser);
//				udi.getUser(null)
				break;
			}
			case LIST_USERS: {
				List<BankAccount> bankAccounts = udi.getPendingApplications();
				int count = 0;
				System.out.println("List of bank accounts pending approval: ");
				for (BankAccount bankAccount : bankAccounts) {
					System.out.println(count+": "+ bankAccount.getUserName() + " " + bankAccount.getAccountName());
					count++;
				}
				System.out.println("Select the account number you would like to approve: ");
				Scanner sc = new Scanner(System.in);
				String userInput = sc.nextLine();
				String userName = bankAccounts.get(Integer.parseInt(userInput)).getUserName();
				String accountName = bankAccounts.get(Integer.parseInt(userInput)).getAccountName();
				udi.approveApplication(userName, accountName); 
				break;
			}
			case LIST_USER_ACCOUNTS: {
				Screen screen = new UserNameScreen();
				String userName = (String) screen.getUserInput(conScan);
				User user = bank.getUser(userName);
				for (BankAccount account : user.getAccounts()) {
					System.out.println(account.getAccountName());
				}
				break;
			}
			}
		}
		conScan.close();

		System.out.println("done");

	}

}
