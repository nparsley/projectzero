package com.example;

import java.util.Scanner;

public class BankAccount {

	private String userName;
	private String accountName;
	private double accountBalance;

	private boolean isApproved = false;

	public BankAccount(String userName, String accountName) {
		this.userName = userName;
		this.accountName = accountName;
//		this.accountBalance = accountBalance;
//		this.isApproved = isApproved;
	}
	

	@Override
	public String toString() {
		return "BankAccount [userName=" + userName + ", accountName=" + accountName + ", accountBalance="
				+ accountBalance + "]";
	}


	public String getAccountName() {
		return accountName;
	}


	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}


	public double getAccountBalance() {
		return accountBalance;
	}


	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}


	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}


	public boolean withdrawAmount(double amount) {
		if (amount >= 0.0 && amount <= accountBalance) {
			accountBalance -= amount;
			return true;
		}

		return false;
	}

	public boolean depositAmount(double amount) {
		if (amount >= 0) {
			accountBalance += amount;
			return true;
		}
		return false;
	}
	

	public boolean transferAmount(double amount, BankAccount otherAccount) {

		if (otherAccount.isApproved() && withdrawAmount(amount)) {
			return otherAccount.depositAmount(amount);
		}
		return false;
	}

	public void approveAccount() {
		this.isApproved = true;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
