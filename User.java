package com.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

// add methods to insert user info to insert into table
//use suer object to invoke the insert methods to add records to table (part of user class)
//estab jdbc section
public abstract class User {
	protected String userName;
	protected String name;
	protected String password;
	protected boolean isRegistered = false;
	protected boolean isLoggedIn = false;
//	protected List<BankAccount> accounts = new ArrayList<>();
	private String userType;

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public User(String name, String userName, String password, String userType) {
		// need to add user type attribute
//		super();
		this.name = name;
		this.userName = userName;
		this.password = password;
		this.userType = userType;
//		this.isRegistered = isRegistered;
//		this.isLoggedIn = isLoggedIn;
	}

	public User() {
		// TODO Auto-generated constructor stub
	}

	public BankAccount applyAccount(String accountName) {
		BankAccount account = new BankAccount(accountName, accountName);
//		accounts.add(account);
		return account;
	}

	public BankAccount getAccount(String name) {
//		for (BankAccount account : accounts) {
//			if (account.getAccountName().equals(name)) {
//				return account;
//			}
//		}
		return null;
	}

	public boolean deleteAccount(String name) {
		BankAccount account = getAccount(name);
		if (account != null) {
//			accounts.remove(account);
			return true;
		}
		return false;
	}

	public List<BankAccount> getAccounts() {
		return null;
	}

	public String getUserName() {
		return userName;
	}
	
	public boolean isAdmin() {
		return userType.toLowerCase().equals("a");
	}
	
	
	public boolean isEmployee() {
		return userType.toLowerCase().equals("e") || userType.toLowerCase().equals("a");
	}
	
	public boolean isCustorAd() {
		return userType.toLowerCase().equals("c") || userType.toLowerCase().equals("a");
	}

//	public void setUserName(String userName) {
//		this.userName = userName;
//	}

	public String getName() {
		return name;
	}

//	public void setName(String name) {
//		this.name = name;
//	}

	public String getPassword() {
		return password;
	}

//	public void setPassword(String password) {
//		this.password = password;
//	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	@Override
	public String toString() {
		return "User [userName=" + userName + ", name=" + name + ", password=" + password + ", isRegistered="
				+ isRegistered + ", isLoggedIn=" + isLoggedIn + "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}



}
