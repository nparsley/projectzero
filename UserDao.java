package com.example.dao;

import java.util.List;

import com.example.User;

public interface UserDao {
	
	void insertUser(String name, String userName, String password, char userType);
	void insertUserObject(User user);
	List<User> getAllUsers();

}
