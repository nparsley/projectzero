package com.example.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.example.Administrator;
import com.example.BankAccount;
import com.example.Customer;
import com.example.Employee;
import com.example.User;

public class UserDaoImpl implements UserDao {

	private static final String URL = "jdbc:postgresql://septjava2109.csoaobqjmqz7.us-east-2.rds.amazonaws.com:5432/projectzerodb";
	private static final String username = "projectzerouser";
	private static final String sqlPass = "password";

	@Override
	public void insertUserObject(User user) {
		// TODO Auto-generated method stub
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String check = "select userName from users where userName = '" + user.getUserName() + "'";
			// query seql server to see if any value records of username
			if (con.prepareStatement(check).executeQuery().getRow() >= 1) {
				// getRow if row = 0 no user w/ that un and if >= 1
				return;
			}

			String sql = "insert into users(name, userName, pwd, userType) values(?,?,?,?)";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, user.getName());

			prepare.setString(2, user.getUserName());

			prepare.setString(3, user.getPassword());

			prepare.setString(4, user.getUserType() + "");

			int changed = prepare.executeUpdate();
			System.out.println("num of rows changed: " + changed);

		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		List<User> users = new ArrayList<User>();
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from users(name, userName, pwd, userType) values(?,?,?,?)";
//			String sql = "select (userName) from users";

			PreparedStatement prepare = con.prepareStatement(sql);

			ResultSet userList = prepare.executeQuery(sql);

			while (userList.next()) {
//				String name, String userName, String password, String userType
				String name = userList.getString("name");
				String userName = userList.getString("userName");
				String pwd = userList.getString("pwd");
				String userType = userList.getString("userType");

				switch (userType) {
				case "C": {
					users.add(new Customer(name, userName, pwd, userType));
					break;
				}
				case "E": {
					users.add(new Employee(name, userName, pwd, userType));
					break;
				}
				case "A": {
					users.add(new Administrator(name, userName, pwd, userType));
					break;
				}
				default: {
					break;
				}
				}
			}
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return users;

	}

	public User getUser(String userName) {
		// TODO Auto-generated method stub
		List<User> users = new ArrayList<User>();
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select name, pwd, userType from users where userName = ?";
//			String sql = "select (userName) from users";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);
			ResultSet userList = prepare.executeQuery();
			userList.next();
			String name = userList.getString("name");
			String pwd = userList.getString("pwd");
			String userType = userList.getString("userType");

			userType = userType.toLowerCase();
			switch (userType) {
			case "c": {
				return new Customer(name, userName, pwd, userType);
			}
			case "e": {
				return new Employee(name, userName, pwd, userType);
			}
			case "a": {
				return new Administrator(name, userName, pwd, userType);
			}
			default: {
				return null;
			}
			}

		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public void insertUser(String name, String userName, String password, char userType) {
// TODO Auto-generated method stub

		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "insert into users(name, userName, pwd, userType) values(?,?,?,?)";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, name);

			prepare.setString(2, userName);

			prepare.setString(3, password);

			prepare.setString(4, userType + "");

			int changed = prepare.executeUpdate();
			System.out.println("num of rows changed: " + changed);

		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public List<BankAccount> getPendingApplications() {
// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		List<BankAccount> bankAccounts = new ArrayList<BankAccount>();

		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from bankaccount where isApproved = false; ";

			Statement prepare = con.createStatement();

			ResultSet baList = prepare.executeQuery(sql);

			while (baList.next()) {
//				String userName, String accountName
				String userName = baList.getString("userName");
				String accountName = baList.getString("accountName");

				BankAccount bankAccount = new BankAccount(userName, accountName);
				bankAccounts.add(bankAccount);

			}
		} catch (SQLException e) {
// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bankAccounts;

	}

	public BankAccount openBankAccount(String userName, String accountName) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		BankAccount bankAccount = new BankAccount(userName, accountName);

		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "insert into bankaccount (userName, accountName, accountBalance, isApproved) values(?,?,?,?)";

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);

			prepare.setString(2, accountName);

			prepare.setDouble(3, 0);

			prepare.setBoolean(4, false);

			int changed = prepare.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bankAccount;

	}

	public boolean accountActions(String userName) {
		String userInput = "0";
		List<BankAccount> accounts = new ArrayList<BankAccount>();
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from bankAccount where isApproved = true and userName = ?";
			// pass in username plus accountname sql satement

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);
//			System.out.println(sql);

			ResultSet accountList = prepare.executeQuery();

			int index = 0;
			System.out.println("Please select an account: ");
			while (accountList.next()) {
//				String userName, String accountName
				String accountName = accountList.getString("accountName");
//				String userName = accountList.getString("userName");
				double accountBalance = accountList.getDouble("accountBalance");
				BankAccount ba = new BankAccount(userName, accountName);
				ba.setAccountBalance(accountBalance);
				accounts.add(ba);
				System.out.print(index);
				System.out.println(accountName);
				index++;
			}
			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();

			BankAccount account = accounts.get(Integer.parseInt(userInput));
			System.out.println("How much would you like to deposit? ");
//			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();
			account.depositAmount(Double.parseDouble(userInput));
			sql = "update bankAccount set accountBalance = ? where accountName = ?";
			prepare = con.prepareStatement(sql);
			prepare.setDouble(1, account.getAccountBalance());
			prepare.setString(2, account.getAccountName());
			prepare.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return true;
	}

	public boolean withdrawActions(String userName) {
		String userInput = "0";
		List<BankAccount> accounts = new ArrayList<BankAccount>();
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from bankAccount where isApproved = true and userName = ?";
			// pass in username plus accountname sql satement

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);
//			System.out.println(sql);

			ResultSet accountList = prepare.executeQuery();

			int index = 0;
			System.out.println("Please select an account: ");
			while (accountList.next()) {
//				String userName, String accountName
				String accountName = accountList.getString("accountName");
//				String userName = accountList.getString("userName");
				double accountBalance = accountList.getDouble("accountBalance");
				BankAccount ba = new BankAccount(userName, accountName);
				ba.setAccountBalance(accountBalance);
				accounts.add(ba);
				System.out.print(index);
				System.out.println(accountName);
				index++;
			}
			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();

			BankAccount account = accounts.get(Integer.parseInt(userInput));
			System.out.println("How much would you like to withdraw? ");
//			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();
			boolean success = account.withdrawAmount(Double.parseDouble(userInput));
			if (success) {
				sql = "update bankAccount set accountBalance = ? where accountName = ?";
				prepare = con.prepareStatement(sql);
				prepare.setDouble(1, account.getAccountBalance());
				prepare.setString(2, account.getAccountName());
				prepare.execute();
			} else {
				System.out.println("invalid amount");
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return true;
	}

	public BankAccount printList() {
		String userInput = "0";
		List<BankAccount> accounts = new ArrayList<BankAccount>();
		BankAccount account = null;
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from bankAccount where isApproved = true";
			// pass in username plus accountname sql satement

			PreparedStatement prepare = con.prepareStatement(sql);

//			System.out.println(sql);

			ResultSet accountList = prepare.executeQuery();

			int index = 0;
			System.out.println("Please select an account: ");
			while (accountList.next()) {
//				String userName, String accountName
				String accountName = accountList.getString("accountName");
				String userName = accountList.getString("userName");
				double accountBalance = accountList.getDouble("accountBalance");
				BankAccount ba = new BankAccount(userName, accountName);
				ba.setAccountBalance(accountBalance);
				accounts.add(ba);
				System.out.print(index);
				System.out.println(accountName);
				index++;
			}
			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();

			account = accounts.get(Integer.parseInt(userInput));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return account;
	}
	
	
	public BankAccount deleteAccount() {
		String userInput = "0";
		List<BankAccount> accounts = new ArrayList<BankAccount>();
		BankAccount account = null;
		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "select * from bankAccount where isApproved = true";
			// pass in username plus accountname sql satement

			PreparedStatement prepare = con.prepareStatement(sql);

//			System.out.println(sql);

			ResultSet accountList = prepare.executeQuery();

			int index = 0;
			System.out.println("Please select an account: ");
			while (accountList.next()) {
//				String userName, String accountName
				String accountName = accountList.getString("accountName");
				String userName = accountList.getString("userName");
				double accountBalance = accountList.getDouble("accountBalance");
				BankAccount ba = new BankAccount(userName, accountName);
				ba.setAccountBalance(accountBalance);
				accounts.add(ba);
				System.out.print(index);
				System.out.println(accountName);
				index++;
			}
			Scanner sc = new Scanner(System.in);
			userInput = sc.nextLine();

			account = accounts.get(Integer.parseInt(userInput));
			sql = "delete from bankAccount where accountName = ?";
			prepare = con.prepareStatement(sql);
			prepare.setString(1, account.getAccountName());
//			System.out.println(prepare.toString());
			prepare.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return account;
	}

// add 
	public boolean approveApplication(String userName, String accountName) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub

		try (Connection con = DriverManager.getConnection(URL, username, sqlPass)) {

			String sql = "update bankAccount set isApproved = true where userName = ? and accountName = ?;";
			// pass in username plus accountname sql satement

			PreparedStatement prepare = con.prepareStatement(sql);
			prepare.setString(1, userName);

			prepare.setString(2, accountName);
			prepare.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;

	}

}

// insert into users (userId, userName, pwd, userType) 