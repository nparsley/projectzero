package com.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class MenuScreen implements Screen {
	enum Action{
		ADD_USER,
		APPLY_ACCOUNT,
		APPROVE_ACCOUNT,
		DELETE_ACCOUNT,
		PRINT_ACCOUNT_INFO,
		TRANSFER_FUNDS,
		WITHDRAW_FUNDS,
		DEPOSIT_FUNDS,
		LIST_USERS,
		LIST_USER_ACCOUNTS,
		LOGIN_USER,
		QUIT
	}

	@Override
	public Action getUserInput(Scanner conScan) {
		Action action = null;
		while (action == null) {

			System.out.println("Please make a selection:");
			System.out.println("1. Add user");
			System.out.println("2. Open account");
			System.out.println("3. Approve account");
			System.out.println("4. Delete account");
			System.out.println("5. Print account info");
			System.out.println("6. Transfer funds");
			System.out.println("7. Withdraw funds");
			System.out.println("8. Deposit funds");
			System.out.println("9. List users");
			System.out.println("10. List user accounts");
			System.out.println("11. Login user");
			System.out.println("12. Quit");

			System.out.println();
			System.out.print("Selection: ");

			String selectionStr = conScan.nextLine();

			try {
				int selection = Integer.parseInt(selectionStr);
				switch (selection) {
				case 1:
					action = Action.ADD_USER;
					break;
				case 2:
					action = Action.APPLY_ACCOUNT;
					break;
				case 3:
					action = Action.APPROVE_ACCOUNT;
					break;
				case 4:
					action = Action.DELETE_ACCOUNT;
					break;
				case 5:
					action = Action.PRINT_ACCOUNT_INFO;
					break;
				case 6:
					action = Action.TRANSFER_FUNDS;
					break;
				case 7:
					action = Action.WITHDRAW_FUNDS;
					break;
				case 8:
					action = Action.DEPOSIT_FUNDS;
					break;
				case 9:
					action = Action.LIST_USERS;
					break;
				case 10:
					action = Action.LIST_USER_ACCOUNTS;
					break;
				case 11:
					action = Action.LOGIN_USER;
					break;
				case 12:
					action = Action.QUIT;
					break;
				default:
					throw new NumberFormatException();
				}
			} catch (NumberFormatException e) {
				System.out.println("Invalid selection, please try again.");
			}
		}
		return action;
	}

//	public void enterAge(Scanner conScan) {
//
//		System.out.println("enter your age:");
//		try {
//			int age = conScan.nextInt();
//			conScan.nextLine();
//		} catch (InputMismatchException e) {
//			System.out.println("That wasn't a number, try again");
//			conScan.nextLine(); // sometimes you will have to put an empty nextLine() to prevent it skipping the
//								// next console input
//			enterAge(conScan);
//		}
//	}

}
