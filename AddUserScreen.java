package com.example;

import java.util.Scanner;

public class AddUserScreen implements Screen {

	@Override
	public User getUserInput(Scanner conScan) {
		// TODO Auto-generated method stub

		User user = null;
		while (user == null) {
			System.out.print("What type of user (C)ustomer, (E)mployee, (A)dministrator: ");
			String userType = conScan.nextLine();

			System.out.print("Please enter name: ");
			String name = conScan.nextLine();

			System.out.print("Please enter username: ");
			String username = conScan.nextLine();

			System.out.print("Please enter password: ");
			String password = conScan.nextLine();

			if (!userType.equalsIgnoreCase("C") && !userType.equalsIgnoreCase("A") && !userType.equalsIgnoreCase("E")) {
				System.out.println("Invalid user type, please try again.");
			} else if (name.isEmpty()) {
				System.out.println("Name cannot be empty, please try again.");
			} else if (username.isEmpty()) {
				System.out.println("Username cannot be empty, please try again.");
			} else if (password.length() < 5) {
				System.out.println("Password must be at least 5 characters, please try again.");
			} else {
//				user = new User(name, username, password);
				if (userType.equalsIgnoreCase("C")) {
					user = new Customer(name, username, password, userType);
				} else if (userType.equalsIgnoreCase("E")) {
					user = new Employee(name, username, password, userType);
				} else if (userType.equalsIgnoreCase("A")) {
					user = new Administrator(name, username, password, userType);
				}
			}
		}
		return user;
	}

}
