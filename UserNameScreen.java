package com.example;

import java.util.Scanner;

public class UserNameScreen implements Screen {

	@Override
	public String getUserInput(Scanner conScan) {
		// TODO Auto-generated method stub
		
		String userName = null;
		while (userName == null) {
			System.out.print("Please enter username: ");
			userName = conScan.nextLine();
			if(userName.isBlank()) {
				System.out.println("Username cannot be blank, please try again.");
				userName = null;
			}
		}
		return userName;
	}

}
