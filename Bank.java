package com.example;
import java.util.Scanner;


import java.util.ArrayList;
import java.util.List;

public class Bank {

//	private List<BankAccount> ledger = new ArrayList<BankAccount>();
	private List<User> users = new ArrayList<>();
	
	
	public void addUser(User user) {
		users.add(user);
	}
	
	public void loginUser(User user) {
		return;
	}
	
	public User getUser(String userName) {
		for (User user : users) {
			if (user.getUserName().equals(userName)) {
				return user;
			}
		}
		return null;
	}
	
	public List<User> getUsers() {
		return users;
	}
}
