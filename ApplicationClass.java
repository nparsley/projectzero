package com.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ApplicationClass {
	private int applicationId;
	private String userId;
	private boolean isApproved = false;

	public int getApplicationId() {
		return applicationId;
	}

	@Override
	public String toString() {
		return "ApplicationClass [applicationId=" + applicationId + ", userId=" + userId + ", isApproved=" + isApproved
				+ "]";
	}

	public ApplicationClass(String userId) {
//		super();
//		this.applicationId = applicationId;
		this.userId = userId;
//		this.isApproved = isApproved;
	}

	// public void setApplicationId(int applicationId) {
//		this.applicationId = applicationId;
//	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public boolean isApproved() {
		return isApproved;
	}

	public void setApproved(boolean isApproved) {
		this.isApproved = isApproved;
	}

	
	// take info from screen then generate object pass into database -- will need database methods to retrieve
//	public void insertApplication(String userId) {
//		// TODO Auto-generated method stub
//
//		try (Connection con = DriverManager.getConnection(URL, username, password)) {
//
//			String sql = "insert into food(foodname, foodrecipe) values(?,?)";
//			// the ? are placeholders that we can inject the values we want to submit to the
//			// database into
//
//			PreparedStatement prepare = con.prepareStatement(sql);
//			prepare.setString(1, f_name); // the setDatatype methods will take the arguments of ? position first, then
//											// value injected at that position
//			prepare.setString(2, f_recipe);
//
//			int changed = prepare.executeUpdate();
//			System.out.println("num of rows changed: " + changed);
//
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

}
