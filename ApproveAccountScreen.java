package com.example;

import java.util.Scanner;

public class ApproveAccountScreen implements Screen {

	@Override
	public String[] getUserInput(Scanner conScan) {
		// TODO Auto-generated method stub
		String approverUserName = null;
		String approveeUserName = null;
		String bankAccountName = null;
		boolean isValid = false;

		while (!isValid) {
			System.out.print("Please enter approver Username: ");
			approverUserName = conScan.nextLine();

			System.out.print("Please enter approvee Username: ");
			approveeUserName = conScan.nextLine();

			System.out.print("Please enter bank account Name: ");
			bankAccountName = conScan.nextLine();

			if (approverUserName.isBlank()) {
				System.out.println("Approver user name cannot be blank, please try again.");
			} else if (approveeUserName.isBlank()) {
				System.out.println("Approvee user name cannot be blank, please try again.");
			} else if (bankAccountName.isBlank()) {
				System.out.println("Bank account name cannot be blank, please try again.");
			} else {
				isValid = true;
			}

		}

		return new String[] { approverUserName, approveeUserName, bankAccountName };
	}

}
