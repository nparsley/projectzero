package com.example;

import java.util.Scanner;

public class ApplyAccountScreen implements Screen {

	@Override
	public String[] getUserInput(Scanner conScan) {
		// TODO Auto-generated method stub
		String userName = null;

		String accountName = null;
		while (userName == null || accountName == null) {
			System.out.print("Please enter username: ");
			userName = conScan.nextLine();
			System.out.print("Please enter an account name: ");
			accountName = conScan.nextLine();

			if (userName.isBlank()) {
				System.out.println("Username cannot be blank, please try again.");
				userName = null;
			}
			if (accountName.isBlank()) {
				System.out.println("Account name cannot be blank, please try again.");
				accountName = null;
			}
		}
		return new String[] { userName, accountName };
	}

}
